# Kinegami
Given a D-H representation of a kinematic chain robot, the program generates a crease pattern that folds into a kinematically equivalent robot.

# User Guide:
Run scripts SphericalKinegami.m, UniversalKinegami.m, PlanarKinegami.m, or CylindricalKinegami.m and change parameters if desired. For more comprehensive understanding, reference supporting functions.

# Updates:
7/5/2021:
Edited JointAssignment.m to include correct value of rs for Prismatic Joints.
Edited Kinegami.m to support plotting for Proximal and Distal Frames. Added new function frameplot.m for frame plotting. Changed manner in which figures are closed in papercut files.

# References
Run any files with Kinegami (preferably Universal and Spherical)
Fixing solvedubins3d.m, specifically looking at lines 40-53 (theta calculations)
